package pl.sda.poznan.sort;

// Zalozenie: mamy liczby z przedzialu 0 - max
public class BucketSort {
    public static void sort(int[] arr) {
        // znalezc max
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }

        }
        // wywowac metode
        sort(arr, max);
    }

    private static void sort(int[] arr, int max) {
        // utworzyc tablice na kubelki
        int[] bucket = new int[max + 1];

        // przejsc for'em przez tablice arr
        // i zliczac ilosc wystapien elementow
        for (int i = 0; i < arr.length; i++) {
            int indeks = arr[i];
            bucket[indeks] += 1;
            // bucket[arr[i]]++; -> skrócony zapis
        }

        // zmienna pomocnicza - jej zadaniem jest przechwytywanie elementow od int j,
        // i wstawiac posortowane liczby do tablicy
        int k = 0;
        // zewnetrzna petla przechodzi przez tablice koszykow
        for (int i = 0; i < bucket.length; i++) {
            // wewnetrzna petla wykonuje sie tyle razy,
            // ile bylo elementow w koszyku
            for (int j = 0; j < bucket[i]; j++) {
                arr[k] = i;
                k++;
            }
        }
    }
}
