package pl.sda.poznan;

public class Person {
    private String name;
    private String surname;
    private int age;


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }


    public int compareTo(Person o) {

        return this.surname.compareTo(o.getSurname());


//
//        if (this.age < o.getAge()) {
//            return -1;
//        } else if (this.age > o.getAge()) {
//            return 1;
//        } else {
//            return 0;
//        }
        // can be replace with "Integer compare:"
//        return Integer.compare(this.age, o.getAge());
    }

}
