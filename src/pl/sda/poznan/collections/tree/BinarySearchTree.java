package pl.sda.poznan.collections.tree;

// Klasa BinarySearchTree jest typu generycznego,
// czyli moze pracowac z dowolnym typem
// typem, ktory implementuje interfejs Comparable
// i klasa BinarySearchTree implementuje interfejs Tree
// <T extends Comparable<T>> - ograniczenie przyjmowanego typu generycznego
// do takiego ktory implementuje interfejs Comparable
// dzieki temu zapisowi na obiekcie root mozemy wywolac metode compareTo()
public class BinarySearchTree<T extends Comparable<T>> implements Tree<T> {

    private Node<T> root;

    @Override
    public void insert(T data) {
        // jestli drzewo jest puste to pierwszy dodawany element staje sie korzeniem
        if (root == null) {
            this.root = new Node<>(data);
        } else {
            insert(data, root);
        }
    }

    private void insert(T data, Node<T> node) {
        // porownujemy wstawiany element z danym wezlem
        // jezeli data.comparaTo(node.getData()) zwroci wartossc ujemna
        // to znaczy, ze musi trafic do lewego poddrzewa (jest mniejszy)
        // w przeciwnym przypadku do prawego
        if (data.compareTo(node.getData()) < 0) {
            // wstawiamy do lewego poddrzewa
            if (node.getLeft() != null) {
                // jezeli dany wezel ma lewego syna,
                // to nie mozemy wstawic w to miejsce
                // musimy rekurencyjnie zejsc w dol - wywolac jeszcze raz ta funkcje
                insert(data, node.getLeft());
            } else {
                // wezel nie ma lewgo syna, wiec wstawiamy w to miejsce
                Node<T> newNode = new Node<>(data);
                node.setLeft(newNode);
            }

        } else {
            if (node.getRight() != null) {
                insert(data, node.getRight());
            } else {
                Node<T> newNode = new Node<>(data);
                node.setRight(newNode);
            }
        }
    }

    @Override
    public void delete(T data) {
        if (root != null) {
            root = delete(root, data);
        }
    }

    private Node<T> delete(Node<T> node, T data) {
        if (node == null) {
            return null;
        }
        // jezeli kasowany element jest mniejszy od wezla, to szukaj go w lewym poddrzewie
        if (data.compareTo(node.getData()) < 0) {
            node.setLeft(delete(node.getLeft(), data));
        } else if (data.compareTo(node.getData()) > 0) {
            // jezeli kasowany element jest wiekszy to szukaj go w prawym poddrzewie
            node.setRight(delete(node.getRight(), data));
        } else {
            // znalezlismy element do usuniecia
            // 3 przypadki:
            // usuwanie liscia
            if (node.getLeft() == null && node.getRight() == null) {
                System.out.println("Removing leaf....");
                return null;
            }

            // usuwanie rodzica z 1 dzieckiem
            if (node.getLeft() == null) {
                System.out.println("Removing right....");
                Node<T> tempNode = node.getRight();
                node = null;
                return tempNode;
            } else if (node.getRight() == null) {
                System.out.println("Removing left...");
                Node<T> tempNode = node.getRight();
                node = null;
                return tempNode;
            }

            // usuwanie rodzica z 2 dzieci
            System.out.println("Removing with 2 items....");
            Node<T> tempNode = getPredecessor(node.getLeft());
            node.setData(tempNode.getData());
            node.setLeft(delete(node.getLeft(), tempNode.getData()));
        }

        return node;
    }

    private Node<T> getPredecessor(Node<T> node) {

        if (node.getRight() != null) {
            return getPredecessor(node.getRight());
        }
        return node;
        // alternatywny zapis
//        if(node.getRight() == null) {
//            return node;
//        } else {
//            return getPredecessor(node.getRight());
//        }
    }

    @Override
    public T getMax() {
        // sprawdzenie czy mamy elementy w drzewie
        if (root == null) {
            return null;
        }
        // sprawdzamy, czy mamy prawy element
        // jezeli korzen nie ma prawego elementu
        // to korzen jest najwiekszym elementem
        if (root.getRight() == null) {
            return root.getData();
        } else {
            // w przeciwnym wypadku
            // musimy zejsc w dol drzewa (rekurencyjnie)
            // wiec wywolujemy funkcje getMax z prawym synem
            return getMax(root);
        }
    }

    // pomocnicza metoda, ktora otrzymuje element drzewa jako argument
    // i sprawdza, czy ma prawego syna -> wtedy kontynuuje rekurencje,
    // az do znalezienia elementu bez prawego syna
    // element bez prawego syna jest najwiekszym elementem
    private T getMax(Node<T> node) {
        if (node.getRight() != null) {
            return getMax(node.getRight());
        }
        return node.getData();
    }


    @Override
    public T getMin() {
        // sprawdzenie czy mamy elementy w drzewie
        if (root == null) {
            return null;
        }

        if (root.getLeft() == null) {
            return root.getData();
        } else {
            return getMin(root.getLeft());
        }
    }

    public T getMin(Node<T> node) {
        if (node.getLeft() != null) {
            return getMin(node.getLeft());
        } else {
            return node.getData();
        }
    }

    @Override
    public void traversal() {
        if (root != null) {
            System.out.println("INORDER");
            inorderTravelsal(root);
//            System.out.println("PREORDER");
//            postorderTravelsal(root);
//            System.out.println("POSTORDER");
//            postorderTravelsal(root);
        }
    }

    @Override
    public boolean contains(T data) {
            return contains(root, data);
        }

    private boolean contains(Node<T> node, T data) {
        if (node == null) {
            return false;
        }
        // znalezlismy szukany element - zwracamy true
        if (node.getData().compareTo(data) == 0) {
            return true;
        } else if (data.compareTo(node.getData()) < 0) {
            // w przeciwnym przypadku, jezeli szukany element jest mniejszy od pola danych aktualnego wezla
            // to szukaj w jego lewym poddrzewie
            return contains(node.getLeft(), data);
        } else {
            // w przeciwnym przypadku element jest wiekszy wiec szukaj w jego prawym poddrzewie
            return contains(node.getRight(), data);
        }
    }

    /**
     * Przejscie "in order" przez drzewo - najpierw lewy syn, potem odwiedz korzen, potem odwiedz prawego syna
     *
     * @param node
     */
    private void inorderTravelsal(Node<T> node) {
        if (node.getLeft() != null) {
            inorderTravelsal(node.getLeft());
        }

        // odwiedz rodzica
        System.out.println(node.getData().toString());

        // odwiedz prawego syna
        if (node.getRight() != null) {
            inorderTravelsal(node.getRight());
        }
    }

    private void preorderTravelsal(Node<T> node) {
        // odwiedz rodzica
        System.out.println(node.getData().toString());


        if (node.getLeft() != null) {
            preorderTravelsal(node.getLeft());
        }

        // odwiedz prawego syna
        if (node.getRight() != null) {
            preorderTravelsal(node.getRight());
        }
    }

    private void postorderTravelsal(Node<T> node) {

        if (node.getLeft() != null) {
            postorderTravelsal(node.getLeft());
        }

        // odwiedz prawego syna
        if (node.getRight() != null) {
            postorderTravelsal(node.getRight());
        }

        // odwiedz rodzica
        System.out.println(node.getData().toString());
    }
}
