package pl.sda.poznan.collections.stack;

/**
 * Stos - struktura danych, gdzie dodajemy na wierzcholek stosu
 * i usuwamy (zdejmujemy) elementy z wierzcholka,
 * Przyklad listy LIFO - Last In First Out
 * Element dodany na koncu bedzie obsluzony jako pierwszy
 * @param <E>
 */
public class Stack<E> {
    private Node<E> top;
    private int size = 0;



    // Odkladanie elementu na stos
    // Zawsze odkladamy na szczyt sotsu
    public void push(E element) {




        if (top == null) {
            top = new Node<>(element);
        } else {
            Node<E> newNode = new Node<>(element);
            newNode.setPrev(top);
            top = newNode;
        }

        size++;
    }

    // Pobieranie elementu ze stosu
    // zawsze pobieramy szczytowy element
    // wiec metoda nie przyjmuje argumentou

    public E pop() {
        --size;
        Node<E> toDelete = top;
        top = top.getPrev();
        return toDelete.getData();
    }

    public boolean isEmpty() {
        return false;
    }

    // Usuwa wszystkie elementy
    public void clear() {
        top = null;
        size = 0;
    }

    // Zwraca element top, bez usuwania go
    public E peek() {return isEmpty() ? null : top.getData();
    // Alternatywny zapis
//        if(top == null) {
//            return null;
//        }
//        return top.getData();


    }
    // metoda ma zwracac czy element wystepuje na stosie
    // jezeli tak to ile elementow od elementu top
    // jezeli szukany element jest na szcycie to zwrocic 1
    // jezeli drugi (zaraz po szczycie to zwrocic 2 itd).
    // jezeli nie to zwracamy -1


    public int search(E element) {
        return -1;
    }

    public int size() {
        return this.size;
    }
}
