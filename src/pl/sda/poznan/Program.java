package pl.sda.poznan;

import pl.sda.poznan.collections.array.ArrayListCustom;

import java.util.LinkedList;
import java.util.List;

public class Program {
    public static void main(String[] args) {
        Person student = new Person();
        student.setAge(20);
        student.setSurname("Kowalski");

        Person kierownik = new Person();
        kierownik.setAge(50);
        kierownik.setSurname("Adamiak");

        int result = student.compareTo(kierownik);
        System.out.println("Wynik porownania to: " + result);
    }
}